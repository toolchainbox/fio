FROM alpine:latest

LABEL maintainer="sebastian.zoll@sva.de"

RUN apk upgrade --no-cache && \
    apk add --no-cache fio

WORKDIR /bench
ENTRYPOINT ["fio"]
